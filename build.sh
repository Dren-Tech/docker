#!/bin/sh

echo "Build base image"

echo "Build Java $1 and Node $2"
docker build -t registry.gitlab.com/drentec/docker/base:java-$1 --build-arg JAVA_VERSION=$1 --build-arg NODE_VERSION=$2 ./base

echo "Build dev-container"
docker build -t registry.gitlab.com/drentec/docker/dev-container:latest ./dev-container